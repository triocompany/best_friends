var user_id;
var gender = 0; // 0 - любой, 1 - жен, 2- муж.
var man = false; // true - выбран мужской.
var female = false; // true - выбран женский.

var today = new Date(); // год сегодня
var age_from = 0;     // требуемый возраст (с)
var year_from = today.getFullYear() - age_from; // нужный год рождения (с)

var age_to = 0;    // требуемый возраст по)
var year_to = today.getFullYear() - age_to; // нужный год рождения (по)

var all_countries = [];
var friends_countries = [];
var all_friends_count;
var show_friends_count = 20;
var i = 1;

var startFrom = 0; /* С какого друга начать */
var inProgress = false; /* Переменная-флаг для отслеживания того, происходит ли в данный момент ajax-запрос. В самом начале даем ей значение false, т.е. запрос не в процессе выполнения */
var sorted_friends; // Все друзья, отсортированные по количеству общих.
var window_height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight; // Высота окна браузера
var friends_container_height = window_height- 270; // Высота div'a с друзьями
var my_friends_container = $("#my_friends_container"); // Контейнер с отображением списка друзей
var nationalties = $(".nationalities"); // Контейнер с отображением списка стран
window.vkAsyncInit = function() {
    VK.init({
        apiId: 5226726
    });
};

setTimeout(function() {
    var el = document.createElement("script");
    el.type = "text/javascript";
    el.src = "//vk.com/js/api/openapi.js";
    el.async = true;
    document.getElementById("vk_api_transport").appendChild(el);
}, 0);

function CommonsFriends(i, ii) {
    if (i.common_count < ii.common_count)
        return 1;
    else if (i.common_count > ii.common_count)
        return -1;
    else
        return 0;
}
function CountCountries(a, b) {
    if (a.value < b.value)
        return 1;
    else if (a.value > b.value)
        return -1;
    else
        return 0;
}

function authInfo(response) {
    if (response.session) {
        // ID пользователя в глобальную переменную user_id.
        user_id = response.session.mid;
        // Список всех стран VK api в массив all_countries.
        VK.Api.call('database.getCountries', {need_all: 1, count:250 }, function(countries) {
            if(countries) {
                $.each( countries.response, function( key, value ) {
                    all_countries[value.cid] = value.title;
                });
            }
        });

        //
        my_friends_container.show();
        $(".friends_show").show();
        $("#logout_button").show();
        $("#login_button").hide();
        $("#my_name").html(response.session.user.last_name+' '+response.session.user.first_name).show();


        VK.Api.call('friends.get', {user_id: user_id}, function(c) {
            if(c.response) {
                all_friends_count = c.response.length;
            }
        });
        startFrom = 0;
        show_friends_count = 20;
        update_list(startFrom, true);
        block_height();
    } else {
        alert('not auth');
    }
}

function update_nationalties() {
    var pro_var = 1;
    nationalties.html('');
    var friends_countries2 = friends_countries;
    var friends_countries3 = [];
    var key;
    var keys = [];
    for(key in friends_countries2) if (friends_countries2.hasOwnProperty(key)) {
        keys.push(key);
        friends_countries3[key] = friends_countries2[key];
    }
    var out = [];
    for (i in friends_countries) {
        var tmp = {
            key: i,
            value: friends_countries[i]
        };
        tmp[i] = tmp.value;
        out.push(tmp);
    }
    out.sort(CountCountries);
    var friends_countries_sorted = [];
    for (i in out) {
        friends_countries_sorted.push(out[i].key);
    }
    for (i = 0; i < 5; i++) {
        if (friends_countries[friends_countries_sorted[i]] != undefined && friends_countries_sorted[i] != 0){
            nationalties.append('' +
            '<div class="nationality">' +
            '<div class="nation_place">'+(i+1)+'</div>' +
            '<div class="nation_info">' +
            '<div class="nation_name">'+all_countries[friends_countries_sorted[i]]+'</div>' +
            '<div id="progressbar'+(i+1)+'" class="progressbars">' +
            '</div>' +
            '</div>' +
            '</div>');
            pro_var = (friends_countries[friends_countries_sorted[i]] / show_friends_count)*100;
            $("#progressbar"+(i+1)).progressbar({value: pro_var});
        }
        if (friends_countries_sorted[i] == 0){
            nationalties.append('' +
            '<div class="nationality">' +
            '<div class="nation_place">'+(i+1)+'</div>' +
            '<div class="nation_info">' +
            '<div class="nation_name">Unknown</div>' +
            '<div id="progressbar'+(i+1)+'" class="progressbars">' +
            '</div>' +
            '</div>' +
            '</div>');
            pro_var = (friends_countries[friends_countries_sorted[i]] / show_friends_count)*100;
            $("#progressbar"+(i+1)).progressbar({value: pro_var});
        }
    }
}

function update_list(startFrom, clear) {
    inProgress = true;
    var exe_code = 'return API.friends.getMutual({"source_uid":'+user_id+',"target_uids":API.friends.get({"user_id":'+user_id+', "fields":"sex,bdate"})@.uid});';
    if (age_to!=0 || gender!=0 ){
        exe_code =
            'var friends = API.friends.get({"user_id":'+user_id+', "fields":"sex,bdate", "v":"3.0"});' +
            'var current_ids = "";' +
            'var i = 1;' +
            'var birthday = 1;' +
            'var byear = 1;' +
            'var itogo = parseInt(friends.length);' +
            'while (parseInt(i) <= itogo) {';
            if (age_to!=0) {
                exe_code +=
                    'if ( friends[i].bdate ) {' +
                    'if ( friends[i].bdate.length > 7 ) {' +
                    'birthday = friends[i].bdate;' +
                    'if (birthday.length == 8){ byear = birthday.substr(4,4);}' +
                    'if (birthday.length == 9){ byear = birthday.substr(5,4);}' +
                    'if (birthday.length == 10){ byear = birthday.substr(6,4);}';
            }
                        exe_code +=
                        'if (';
                            if (age_to!=0){ exe_code += ' byear >= '+year_to+' && byear <= '+year_from+' &&';}
                            if (gender!=0){exe_code += ' parseInt(friends[i].sex) == '+gender+' &&';}
                            exe_code = exe_code.slice(0, -2);
                        exe_code += ') ';

            exe_code += '{ current_ids = current_ids + friends[i].uid +  ","; };';
            if (age_to!=0){
                exe_code +=
                    '};' +
                '};';
            }
            exe_code += 'i = i+1;' +
            '};' +
            'current_ids = current_ids.substr(0, current_ids.length-1);' +
            'return API.friends.getMutual({"source_uid":'+user_id+',"target_uids":current_ids});';
    }

    VK.Api.call('execute', {code: exe_code}, function(exe) {
        inProgress = false;
        if (exe.response) {
            sorted_friends = exe.response;
            all_friends_count = sorted_friends.length;
            sorted_friends.sort(CommonsFriends);
            $("#test_res").html(sorted_friends);
            i = 1;
            var sorted_20 = sorted_friends.slice(startFrom, startFrom+20);
            var sorted_20_ids = '';
            var sorted_20_friends = [];
            $.each( sorted_20, function( key, value ) {
                if (value.common_count == undefined) {
                    value.common_count = 0;
                }
                sorted_20_friends[value.id] = value.common_count;
                sorted_20_ids += value.id+',';
            });
            sorted_20_ids = sorted_20_ids.slice(0, -1);
            var container = $("#my_friends");
            VK.Api.call('users.get', {user_ids: sorted_20_ids, fields:'sex, country, photo_100, photo_200_orig, has_mobile, contacts, education, online, relation, last_seen, status, universities'}, function(res) {
                if (clear){
                    container.html('');
                    friends_countries = [];
                }
                $.each( res.response, function(key, value ) {
                    if ( value.uid != user_id)
                    {
                        if (value.university_name == undefined) {
                            value.university_name = '';
                        }
                        if (value.country != undefined || value.country != '') {
                            if (value.country == undefined || value.country == ''){
                                value.country = 0;
                            }
                            if (friends_countries[value.country] == undefined){
                                friends_countries[value.country] = 1;
                            }
                            else{
                                friends_countries[value.country]++;
                            }
                        }
                        container.append('<div class="friend_block"><div class="avatar_block"><img class="avatar" src="'+value.photo_100+'"></div><div class="info_block"><a class="name" href="http://vk.com/id'+value.uid+'">'+value.first_name+' '+value.last_name+'</a><p class="study">'+value.university_name+'</p><p class="common_friends">'+sorted_20_friends[value.uid]+' common friends</p></div></div>');
                    }
                 });
                update_nationalties();
            });
        }
    });
}
function block_height() {
    window_height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
    friends_container_height = window_height-270;
    $( "#my_friends_container").height(friends_container_height);
}
VK.Auth.getLoginStatus(authInfo);
VK.UI.button('login_button');

$( window ).resize(function() {
    block_height();
});
$(document).ready(function(){
    $( "#format" ).buttonset();
    /* Все ли пользователи загружены */
    var all_friends = false;
    // Указываем class блока div где будет ползунок.
    var slider_age = $(".slider");
    slider_age.slider({
        animate: true, // Анимация ползунка
        range: true, // Фон пути ползунка, если это свойство убрать, то синей линии не будет.
        values: [0,0], // Значение по умолчанию.
        min: 0, // Минимальная сумма.
        max: 100, // Максимальная сумма.
        step: 1, // Шаг диапазона.

        stop: function(event, ui) {
            $("#slider-min").val(slider_age.slider("values",0));
            $("#slider-max").val(slider_age.slider("values",1));
        },

        // Вывод диапазона
        slide: function( event, ui ) {
            //$( "#slider-result" ).html(ui.value);

            $("#slider-min").html(slider_age.slider("values",0));
            $("#slider-max").html(slider_age.slider("values",1));
        },

        // Вывод диапазона в поле input
        change: function(event, ui) {
            $("#slider-min").html(slider_age.slider("values",0));
            $("#slider-max").html(slider_age.slider("values",1));

            // Запись выбранного диапазона и формирование нужных лет.
            age_from = slider_age.slider("values",0);     // требуемый возраст (с)
            year_from = today.getFullYear() - age_from; // нужный год рождения (с)

            age_to = slider_age.slider("values",1);    // требуемый возраст (по)
            year_to = today.getFullYear() - age_to; // нужный год рождения (по)

            startFrom = 0;
            show_friends_count = 20;
            update_list(startFrom, true); // обновить список друзей, с очищением.
        }

    });

    var handles = slider_age.children('a');
    var handles_count = 1;
    $.each( handles, function( value ) {
        if (handles_count == 1) {
            $(this).append('<div id="slider-min" class="handles">0</div>');
        }
        else{
            $(this).append('<div id="slider-max" class="handles">-</div>');
        }
        handles_count++;
    });

    $("#check1").click(function() {
        if (!man) {
            if (female){gender = 0;}
            else{gender=2;}
            man = true;
        }
        else {
            if (female){gender = 1;}
            else{gender=0;}
            man = false;
        }
        startFrom = 0;
        show_friends_count = 20;
        update_list(startFrom, true);
    });
    $("#check2").click(function() {
        if (!female) {
            if (man){ gender = 0; }
            else{gender=1;}
            female = true;
        }
        else {
            if (man){gender = 2;}
            else{gender=0;}
            female = false;
        }
        startFrom = 0;
        show_friends_count = 20;
        update_list(startFrom, true);

    });

    $("#logout_button").click(function() {
        my_friends_container.hide();
        $("#my_name").hide();
        $("#login_button").show();
        $("#logout_button").hide();
        $(".friends_show").hide();
    });

    my_friends_container.scroll(function() {
        all_friends = show_friends_count >= all_friends_count;
        if (all_friends){
            show_friends_count = sorted_friends.length;
            update_nationalties();
        }
        if(my_friends_container.scrollTop() + friends_container_height >= show_friends_count*164 - 300 && !inProgress && !all_friends) {
            startFrom += 20;
            show_friends_count = startFrom+20;
            if (show_friends_count > all_friends_count){
                show_friends_count = sorted_friends.length;
            }
            inProgress = true;
            update_list(startFrom, false);
        }
    });
});