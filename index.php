<html>
<head>
    <title>BEST FRIENDS</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>

    <link rel="stylesheet" href="/app/css/jquery-ui-1.10.3.custom.min.css" type="text/css" >
    <link rel="stylesheet" href="/app/css/fonts.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/app/css/main.css" type="text/css" media="screen">

    <script type="text/javascript" src="/app/js/jquery-2.2.0.min.js"></script>
    <script type="text/javascript" src="/app/js/jquery-ui-1.10.3.custom.min.js"></script>

    <script src="//vk.com/js/api/openapi.js" type="text/javascript"></script>
</head>
<body>
<div id="vk_api_transport"></div>
<div class="top_menu">
    <div id="logout_button" onclick="VK.Auth.logout();" style="display: none;">
        Logout
    </div>
    <div id="login_button" class="login_button" onclick="VK.Auth.login(authInfo, 3);">
        войти
    </div>
    <p id="my_name" class="login_button"></p>
</div>
<div class="right_menu friends_show">
    <p class="right_title">NATIONALITIES:</p>
    <div class="nationalities">
        <div class="nationality">
            <div class="nation_place">1</div>
            <div class="nation_info">
                <div class="nation_name">Казахстан</div>
                <div id="progressbar1"></div>
            </div>
        </div>
        <div class="nationality">
            <div class="nation_place">2</div>
            <div class="nation_info">
                <div class="nation_name">США</div>
                <div id="progressbar2"></div>
            </div>
        </div>
        <div class="nationality">
            <div class="nation_place">3</div>
            <div class="nation_info">
                <div class="nation_name">Unknown</div>
                <div id="progressbar3"></div>
            </div>
        </div>
    </div>
</div>
<div class="top_filter friends_show">
    <div class="age_slider">
        <p class="filter_name">AGE:</p>
        <div class="slider"></div>
    </div>
    <div class="gender_filter">
        <p class="filter_name">GENDER:</p>
        <div id="format">
            <input type="checkbox" id="check1" name="male"><label for="check1">MALE</label>
            <input type="checkbox" id="check2" name="female"><label for="check2">FEMALE</label>
        </div>
    </div>

</div>

<div id="my_friends_container">
    <div id="my_friends"></div>
</div>


<script type="text/javascript" src="/app/js/main.js"></script>
</body>
</html>


